﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch spriteBatch;
       //instancia de controlador y contexto
        private Class.Controlador _controlador;
        private ContentManager _context;
       //Definicion de alto y largo de la pantalla
        const int _Pantalla_Alto = 600;
        const int _Pantalla_Largo = 600;
        public float tiempo;
        private SpriteFont font;
        

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = _Pantalla_Largo;
            _graphics.PreferredBackBufferHeight = _Pantalla_Alto;
            _graphics.IsFullScreen = false;   
            
            Window.Title = "Juego";
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this._context = Content;
            _controlador = new Class.Controlador(_Pantalla_Alto, _Pantalla_Largo, _context, spriteBatch);
            font = Content.Load<SpriteFont>("Font");            
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            this._controlador.Actualizar(tiempo);
            tiempo = (float)gameTime.ElapsedGameTime.TotalSeconds;
            this._controlador.Actualizar(tiempo);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            
            // TODO: Add your drawing code here
            spriteBatch.Begin();
           
            this._controlador.Dibujar();
            spriteBatch.DrawString(font,  this._controlador.timer.ToString("F1"), new Vector2(435, 85), Color.Black);
            spriteBatch.DrawString(font, this._controlador.contPuntos.ToString("F1"), new Vector2(90, 95), Color.Black);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
