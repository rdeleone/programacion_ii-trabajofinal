﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
    public class Letra:Entidad
    {
        public Letra(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color) : base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color)
        {          
            this.Inicializar(cm, sb);
            this.AsignarValores();
        }
        
        private void Inicializar(ContentManager cm, SpriteBatch sb)
        {
            contPuntos = 0;


            this.listaTexturas.Add(cm.Load<Texture2D>("gota1"));
            this.listaTexturas.Add(cm.Load<Texture2D>("gota0"));
            this.listaTexturas.Add(cm.Load<Texture2D>("gota2"));
            this.img_Tick = (cm.Load<Texture2D>("Tick"));

            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
            

            for (int i = 0; i < 29; i++)
            {
                Random r = new Random();
                int rand = r.Next(0, 3);
                this.Imagen = this.listaTexturas[rand]; // crea los cuadrados de colores de forma random 

            }

            var random = new Random();
            this.posicion.X = random.Next(100, 500);  // que los posiciones de forma random 
            this.posicion.Y = -1 * (random.Next(10, 50));

            this.Velocidad = random.Next(1, 3); // la velocidad con la que bajan 

          
            this.Largo = this.Imagen.Width; 
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            this.posicion.Y += velocidad_y * this.Velocidad;
            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
        }
        public override void Dibujar()
        {
            if (this.EstadoEntidad == Estado.COLISION)
            {
                this.Imagen = this.img_Tick;
                contPuntos += 5;               
            }
            else
            {
                base.Dibujar();
            }

        }
        public override void Reinicializar() // para reinicializar la posicion de las letras si no son agarradas o son erroneas
        {
            var random = new Random();

            this.posicion.X = random.Next(100, 400);
            this.posicion.Y = -1 * (random.Next(10, 50));

            this.Velocidad = random.Next(1, 3);

            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
        }
        public void AsignarValores()
        {
            
           // lograr asignar valores a las posiciones de la lista de texturas 
        }

    }
}
