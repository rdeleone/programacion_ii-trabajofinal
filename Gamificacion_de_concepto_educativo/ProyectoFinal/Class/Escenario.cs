﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
    public class Escenario:Entidad
    {
        public Escenario(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color) : base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color)
        {
            this.Inicializar(cm,sb);
        }
        private void Inicializar(ContentManager cm, SpriteBatch sb)
        {
            this.img_Escenario = (cm.Load<Texture2D>("Escenario"));
            this.Imagen = this.img_Escenario;
            this.posicion.X = 0;
            this.posicion.Y = 0;             
        }
        public override void Mover(float velocidad_x, float velociada_y)
        {
            velociada_y = 0;
            velocidad_x = 0;
        }

        public override void Reinicializar()
        {

        }


    }
}
