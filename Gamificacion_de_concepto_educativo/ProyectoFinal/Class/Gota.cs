﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
    public class Gota:Entidad
    {
        Random r = new Random(); 
        public float random;
        

        public Gota(int pantalla_alto,int pantalla_largo,ContentManager cm,SpriteBatch sb,Color color) 
            : base(pantalla_alto,pantalla_largo,cm,sb,color)
        {          
            this.Inicializar(cm, sb);
            
        }
        
        private void Inicializar(ContentManager cm, SpriteBatch sb)
        {
           
           
            this.listaTexturas.Add(cm.Load<Texture2D>("gota 0")); // 0 Rojo
            this.listaTexturas.Add(cm.Load<Texture2D>("gota 1")); // 1 Azul           
            this.listaTexturas.Add(cm.Load<Texture2D>("gota 2")); // 2 Verde
            this.img_Tick = (cm.Load<Texture2D>("Tick"));

            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;

            

            var random = new Random();
            this.posicion.X = random.Next(30, 570);  // que los posiciones de forma random 
            this.posicion.Y =  (random.Next(240, 250));
            this.Velocidad = random.Next(1, 2); // la velocidad con la que bajan 

            num = r.Next(0, 3);
            this.Imagen = this.listaTexturas[num]; // los genere de forma random 
            this.Largo = this.Imagen.Width; 
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            this.posicion.Y += velocidad_y * this.Velocidad;
            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
            
        }
        public override void Dibujar()
        {
            if (this.EstadoEntidad == Estado.COLISION)
            {
                this.Imagen = this.img_Tick;
                          
            }
            else
            {
                base.Dibujar();
            }

        }
        public override void Reinicializar() // para reinicializar la posicion de las letras si no son agarradas o son erroneas
        {
            var random = new Random();
            this.posicion.X = random.Next(30, 570);
            this.posicion.Y = (random.Next(240, 250));
            this.Velocidad = random.Next(1, 3);
            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;

            num = r.Next(0, 3);
            this.Imagen = this.listaTexturas[num];
        }
    }
}
