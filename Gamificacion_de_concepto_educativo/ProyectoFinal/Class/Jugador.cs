﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace ProyectoFinal.Class
{
    public class Jugador:Entidad
    {
        public Jugador(int pantalla_alto, int pantalla_largo, ContentManager cm, SpriteBatch sb, Color color)
            : base(pantalla_alto, pantalla_largo, cm, sb, color) 
        {
        this._velocidad_X = 0;
        this._velocidad_Y = 0;
        this.posicion=new Vector2(300,300);
        this._pantalla_alto=pantalla_alto;
        this._pantalla_largo=pantalla_largo;
        this.spriteBatch = sb;

            this.Inicializar(cm, this.spriteBatch);
            

        }

        private void Inicializar(ContentManager cm, SpriteBatch sb)
        {
            this.listaTexturas.Add(cm.Load<Texture2D>("personaje_Der")); // agrega la img del pj
            this.listaTexturas.Add(cm.Load<Texture2D>("personaje_Iz"));
            
            this.posicion = new Vector2(this._pantalla_largo / 2, this._pantalla_alto - 100);
            this.X = (int)this.posicion.X; 
            this.Y = (int)this.posicion.Y;
            this.Velocidad = 2;
            this.Imagen = this.listaTexturas[0];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            float v = (velocidad_x * this.Velocidad);
            if (((this.posicion.X) + v ) >= 0 && (((this.posicion.X) + this.Largo) + v) <= this._pantalla_largo)
            {
                this.posicion.X += v;
            }
            this.posicion.Y += (velocidad_y) * this.Velocidad;
            int img = 0;
            if (velocidad_x > 0) // si va para la derecha que tome la img 0 la default 
            {
                img = 0; // Entidad / IMG_ENUMERACION / 0 = DERECHA
            }
            if (velocidad_x < 0) // si va para la izquierda que tome la img 1 que seria el pj mirando a la izq
            {
                img = 1; // Entidad / IMG_ENUMERACION / 1 = IZQUIERDA
            }
            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
            this.Imagen = this.listaTexturas[img];
        }
        public override void Reinicializar()
        {
            throw new NotImplementedException();
        }
    }
}
