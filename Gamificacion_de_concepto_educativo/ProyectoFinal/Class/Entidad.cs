﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
     public abstract class Entidad
    {

        #region"enum"
        public enum Estado 
        {
            IDLE = 0,
            MOV=1,
            SALTANDO=2,
            COLISION=3 
        }

        public enum IMG_Direccion
        {

            
            DER = 0,
            IZQ = 1
            
        }
        #endregion

        #region "Variables"
        protected private float _velocidad_X = 0;
        protected private float _velocidad_Y = 0;
        protected private Vector2 posicion;
        protected private int _pantalla_alto;
        protected private int _pantalla_largo;
        public int num;
        protected private List<Texture2D> listaTexturas = new List<Texture2D>();
        protected private SpriteBatch spriteBatch;
        protected private Texture2D img_Escenario;
        protected private Texture2D img_Tick;

        #endregion

        #region "Constructures"
        public int X { get; set; }
        public int Y { get; set; }
        public int Largo { get; set; }
        public int Alto { get; set; }
        public Texture2D Imagen { get; set; }
        public Estado EstadoEntidad { get; set; }
        public Color ColorEntidad { get; set; }

        public float Velocidad { get; set; }
        public Entidad() { }
        public Entidad(int pantalla_alto,int pantalla_largo, ContentManager cm, SpriteBatch sb, Color color)
        {
            this.EstadoEntidad = Estado.IDLE;
            this.spriteBatch = sb;
            this._pantalla_alto = pantalla_alto;
            this._pantalla_alto = pantalla_largo;
            this.listaTexturas = new List<Texture2D>();
            this.ColorEntidad = color;
        }
        #endregion

        #region"Metodos"
        public virtual void Dibujar() 
        {
            this.spriteBatch.Draw(this.Imagen, this.posicion, this.ColorEntidad);
        }
        public abstract void Mover(float velocidad_x, float velocidad_y); // para que cada objeto que herede tenga que declarar este metodo
       
        public virtual void Parar() { }

        public abstract void Reinicializar();
        #endregion

    }
}
