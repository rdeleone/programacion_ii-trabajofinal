﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
    public class Controlador
    {
        private int _pantalla_Alto;
        private int _pantalla_Largo;
        private SpriteBatch spriteBatch;
        private ContentManager _context;
        private float _velocidad_X = 0;
        private float _velocidad_Y = 0;
        private int _cantGotas = 20;
        public float timer;
        public float contPuntos;
        private int _ciclo;

        public List<Gota> Gotas { get; set; }
        private Jugador jugador;
        private Escenario escenario;
        private Nube nube;


        public Controlador() { }
        public Controlador(int pantalla_alto, int pantalla_largo, ContentManager cm, SpriteBatch sb) //Instancio todos los elementos
        {
            this._pantalla_Alto = pantalla_alto;
            this._pantalla_Largo = pantalla_largo;
            this.spriteBatch = sb;
            this._context = cm;
            this.contPuntos = 0;
            this._ciclo = 0;

            this.Gotas = new List<Gota>();
            this.jugador = new Jugador(this._pantalla_Alto, this._pantalla_Largo, this._context, this.spriteBatch, Color.White);
            this.escenario = new Escenario(this._pantalla_Alto, this._pantalla_Largo, this._context, this.spriteBatch, Color.White);
            this.nube = new Nube(this._pantalla_Alto, this._pantalla_Largo, this._context, this.spriteBatch, Color.White);

            this.AgregarEntidad();

        }
        public void BorrarEntidad() { }
        public void Actualizar(float tiempo)
        {
            timer += tiempo;
            this._ciclo += 1;
            this.LeerTeclas();
            this.jugador.Mover(_velocidad_X, _velocidad_Y);// mover al jugador
            this._velocidad_X = 0;
            this._velocidad_Y = 0;

            if (_cantGotas < 6)
            {               
                this.AgregarEntidad();
                _cantGotas += 20;
            }

            



            foreach (Gota g in this.Gotas) // para que se muevan las letras que se crearon en la lista de entidades
            {
                if (g.EstadoEntidad != Entidad.Estado.COLISION) // si la letra no esta en colision
                {
                    g.Mover(0, 1);
                    if (g.Y > this._pantalla_Largo) // si sale por abajo de la pantalla que se reinicalice 
                    {
                        g.Reinicializar();
                    }
                }
            }
            Rectangle colliderJugador = new Rectangle(this.jugador.X + 10, this.jugador.Y + 10, this.jugador.Largo + 10, this.jugador.Alto + 10);

            foreach (Gota g in this.Gotas)
            {

                if (g.EstadoEntidad != Entidad.Estado.COLISION)
                {
                   
                    Rectangle colGota = new Rectangle(g.X, g.Y, g.Largo, g.Alto);
                    if (colliderJugador.Intersects(colGota))
                        {
                        if (nube.num == 0 && g.num == 0)
                        {
                             g.EstadoEntidad = Entidad.Estado.COLISION;
                             contPuntos += 5;
                            _cantGotas -= 1;
                        }
                        if (nube.num == 1 && g.num == 1)
                        {
                            g.EstadoEntidad = Entidad.Estado.COLISION;
                            contPuntos += 5;
                            _cantGotas -= 1;
                        }
                        if (nube.num == 2 && g.num == 2)
                        {
                            g.EstadoEntidad = Entidad.Estado.COLISION;
                            contPuntos += 5;
                            _cantGotas -= 1;
                        }

                    }
                    
                }
            }
        }
    

        public void LeerTeclas()
        {
            if(Keyboard.GetState().IsKeyDown(Keys.Right)) 
            {
                this._velocidad_X = +1.5f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                this._velocidad_X = -1.5f;
            }

        }
        public void Dibujar() 
        {
            
            this.escenario.Dibujar();
            this.jugador.Dibujar();
            this.nube.Dibujar();
            foreach (Entidad e in this.Gotas) // para que se muevan las letras que se crearon en la lista de entidades
            {
                e.Dibujar();
            }
        }
        public void AgregarEntidad()
        {
            
            for (int i = 0; i < 21; i++)
            {
                this.Gotas.Add(new Gota(this._pantalla_Alto, this._pantalla_Largo, this._context, this.spriteBatch, Color.White));                
            }           
        } 
    }
}
