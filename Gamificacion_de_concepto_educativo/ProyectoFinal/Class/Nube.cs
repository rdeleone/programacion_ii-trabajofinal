﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ProyectoFinal.Class
{
    public  class Nube : Entidad
    {
        Random r = new Random();
        public int random;

        public Nube(int pantalla_alto, int pantalla_largo, ContentManager cm, SpriteBatch sb, Color color)
            : base(pantalla_alto, pantalla_largo, cm, sb, color)
        {
            this.Inicializar(cm, sb);

        }
        public void Inicializar(ContentManager cm, SpriteBatch sb)
        {

            this.listaTexturas.Add(cm.Load<Texture2D>("0")); // Rojo
            this.listaTexturas.Add(cm.Load<Texture2D>("1")); // Azul          
            this.listaTexturas.Add(cm.Load<Texture2D>("2")); // Verde

            this.X = (int)this.posicion.X;
            this.Y = (int)this.posicion.Y;
            this.posicion = new Vector2(0, 0);

            for (int i = 0; i < 4; i++) 
            {               
                num = r.Next(i);
            }
            this.Imagen = this.listaTexturas[num];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;

        }
        public override void Dibujar()
        {
            
            base.Dibujar(); 
        }
        public override void Mover(float velocidad_x, float velociada_y) { }

        public override void Reinicializar()
        {
            for (int i = 0; i < 4; i++)
            {
                num = r.Next(i);
            }
            this.Imagen = this.listaTexturas[num];
        }
    }
}
